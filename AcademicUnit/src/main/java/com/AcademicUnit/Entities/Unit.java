package com.academicunit.entities;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.academicunit.entities.audit.UnitDateAudit;

@Entity
@Table(name="unit")
public class Unit extends UnitDateAudit {

	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)  //MYSQL=IDENTITY//ORACLE=SEQUENCE
	private int id;
	
	@Column(name="type")
	private String type;
	
	@Column(name="name")
	private String name;
	
	@Column(name="opendate")
	private String opendate;
	
	@Column(name="remark")
	private String remark;
	
	@Column(name="attachedunit")
	private int attachedunit;
	
	@Column(name="typeid")
	private int typeid;
	
	@ManyToOne
    @JoinColumn(name = "typeid" , insertable=false, updatable=false)
    private UnitType unitType;




	// hibernate need empty constructor
	public Unit() {
		
	}






	public Unit(int id, String type, String name, String opendate, String remark, int attachedunit, int typeid) {
		super();
		this.id = id;
		this.type = type;
		this.name = name;
		this.opendate = opendate;
		this.remark = remark;
		this.attachedunit = attachedunit;
		this.typeid=typeid;
		
	}

	public int getTypeid() {
		return typeid;
	}

	public void setTypeid(int typeid) {
		this.typeid = typeid;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getOpendate() {
		return opendate;
	}


	public void setOpendate(String opendate) {
		this.opendate = opendate;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public int getAttachedunit() {
		return attachedunit;
	}

	public void setAttachedunit(int attechedunit) {
		this.attachedunit = attechedunit;
	}


	
	public UnitType getUnitType() {
		return unitType;
	}


	public void setUnitType(UnitType unitType) {
		this.unitType = unitType;
	}







	
}
