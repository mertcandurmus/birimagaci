package com.academicunit.dataaccess;

import java.util.List;

import com.academicunit.entities.Unit;
import com.academicunit.entities.UnitType;



public interface IUnitDao {

	List<Unit> getAll();
	void add(Unit unit);
	void delete(Unit unit);
	void update(Unit unit);
	Unit getById(int id);
	List<Unit>  getAttechedUnit(int attachedunit);
	List<Unit>  getByType(String type);
	String getType(int type_id);
	String getType2(int id);
	List<UnitType> getAllType();
	
}
