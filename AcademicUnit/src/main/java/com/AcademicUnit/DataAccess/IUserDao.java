package com.academicunit.dataaccess;

import com.academicunit.entities.User;

public interface IUserDao {

	void add(User user);
	void delete(User user);
	void update(User user);

}
