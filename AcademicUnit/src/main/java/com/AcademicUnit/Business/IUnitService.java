package com.academicunit.business;

import java.util.List;

import com.academicunit.entities.Unit;
import com.academicunit.entities.UnitType;

public interface IUnitService {

	
	List<Unit> getAll();
	void add(Unit unit);
	void delete(Unit unit);
	void update(Unit unit);
	Unit getById(int id);
	List<Unit>  getAttechedUnit(int attachedunit);
	List<Unit>  getByType(String type);
	List<UnitType> getAllType();
}
