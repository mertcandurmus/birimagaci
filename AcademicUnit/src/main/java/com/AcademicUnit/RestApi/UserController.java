package com.academicunit.restapi;

import java.net.URI;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import com.academicunit.dataaccess.RoleRepository;
import com.academicunit.dataaccess.UserRepository;
import com.academicunit.entities.Role;
import com.academicunit.entities.RoleName;
import com.academicunit.entities.User;
import com.academicunit.exception.AppException;
import com.academicunit.payload.ApiResponse;
import com.academicunit.payload.JwtAuthenticationResponse;
import com.academicunit.payload.LoginRequest;
import com.academicunit.payload.SignUpRequest;
import com.academicunit.security.JwtTokenProvider;




@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    
    
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getusername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    
    
    
    
    
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
    	
    	//username control
        if(userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "kullanici adi zaten alinmis!"),
                    HttpStatus.BAD_REQUEST);
        }

        //email control
        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "email adres zaten alinmis"),
                    HttpStatus.BAD_REQUEST);
        }

        
        //create user
        User user = new User(signUpRequest.getName(),signUpRequest.getSurname(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), signUpRequest.getPassword());
        //create hash password
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        
        //create role and set to user      
        Role userRole = roleRepository.findByName(RoleName.ROLE_USER).orElseThrow(() -> new AppException("User Role ayarlanamadı."));
        user.setRoles(Collections.singleton(userRole));
        
        //save user 
        userRepository.save(user);
        
        //user roles control
        System.out.println(user.getRoles());
        
        
//        User result = userRepository.save(user);
//        URI location = ServletUriComponentsBuilder
//                .fromCurrentContextPath().path("/api/users/{username}")
//                .buildAndExpand(result.getUsername()).toUri();
        return ResponseEntity.ok(new ApiResponse(true, "Kullanıcı başarılı bir şekilde kaydedildi"));       
}   

    
    
    
//        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
//                .orElseThrow(() -> new AppException("User Role not set."));
//        System.out.println(userRole);
//        user.setRoles(Collections.singleton(userRole));

        
}