package com.academicunit.controllertest;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.academicunit.AcademicUnitApplication;
import org.junit.runners.MethodSorters;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AcademicUnitApplication.class)
@SpringBootTest
public class UnitControllerTest {

	
	private MockMvc mockMvc;
	
	@Autowired
    private WebApplicationContext wac;
	
	
	
	@Before
	public void setup() {
       this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

	}

	
	@Test
	public void e_verifyAllUnitList() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/units").accept(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$", hasSize(26)))
			.andExpect(status().is(200))
			.andDo(print());
		
		
	}
	
	
	@Test
	public void a_verifyUnitById() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/units/1").accept(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").exists())
		.andExpect(jsonPath("$.name").exists())
		.andExpect(jsonPath("$.type").exists())
		.andExpect(jsonPath("$.opendate").exists())
		.andExpect(jsonPath("$.remark").exists())
		.andExpect(jsonPath("$.attachedunit").exists())
		.andExpect(status().is(200))
		.andExpect(jsonPath("$.id").value(1))
		.andExpect(jsonPath("$.name").value("Gazi Universitesi"))
		.andExpect(jsonPath("$.type").value("Yüksekokul"))
		.andExpect(jsonPath("$.opendate").value("01012000"))
		.andExpect(jsonPath("$.remark").value("010120001"))
		.andExpect(jsonPath("$.attachedunit").value(0))
		.andDo(print());
	}
	
	@Test
	public void d_verifyDeleteUnit() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/delete/40")
		.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(200))
		.andDo(print());

	}
	
	@Test
	public void b_verifySaveUnit() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/add")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"name\" : \"Gazi Universitesi Test2\", \"type\" : \"Yüksekokul\", \"opendate\" : \"01\", \"remark\" : \"01\", \"attachedunit\" : \"0\" }")
		.accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(200))
		.andDo(print());
	}
	
	
	
	@Test
	public void c_verifyUpdateUnit() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/update")
        .contentType(MediaType.APPLICATION_JSON)
        .content("{\"id\" : \"21\",\"name\" : \"Gazi Universitesi Test2\", \"type\" : \"Yüksekokul\", \"opendate\" : \"010\", \"remark\" : \"01\", \"attachedunit\" : \"0\" }")
        .accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(200))
		.andDo(print());
	}
}
