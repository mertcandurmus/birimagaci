import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Unit } from '../models/unit';
import { UnitService } from '../services/unit.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-birim-ekle',
  templateUrl: './birim-ekle.component.html',
  styleUrls: ['./birim-ekle.component.css']
})
export class BirimEkleComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private unitService: UnitService, private router: Router) { }


  unitAddForm: FormGroup;

  unit: Unit = new Unit();
  unit2: Unit = new Unit();
  leastUnit: Unit[];
  showMsg;
  types;
  secim;




  createunitAddForm() {

    this.unitAddForm = this.formBuilder.group({
      name: ['', Validators.required],
      type: ['', Validators.required],
      opendate: ['', Validators.required],
      remark: ['', Validators.required],
      attachedunit: ['', Validators.required],
      typeid: ['', Validators.required]
    });
  }

  add() {

      this.unit = Object.assign({}, this.unitAddForm.value);
      // mdegiştiriyorum
      this.unitService.addUnit(this.unit).subscribe(
        data => {
          this.showMsg = true;
          this.getTypeId();
          setTimeout(() => {this.showMsg = false; this.router.navigate(['unit']); }, 2500);
        },
        error => console.log(error));

  }

  getLeastUnit(id: number) {
    this.unitService.getLeastUnit(id).subscribe(data => {
      this.leastUnit = data;
    });
  }

  getAllTypes() {
    this.unitService.getTypes().subscribe(data => {
      this.types = data;
    });
    console.log(this.types);

  }

  getTypeId() {
    this.unit2 = Object.assign({}, this.unitAddForm.value);
    const degisken = this.unit2.type;
    console.log('degisken: ' + degisken);
   // const index = this.types.index(degisken);
    // console.log('index: ' + index);
    const side = this.types[3].id;
    console.log('side: ' + side);
    console.log(Object.keys(this.types));
    console.log(Object.values(this.types)[1][2]);
   // const index = this.types.find(degisken);
  //  console.log('index: ' + index);
  }

  ngOnInit() {
    this.getAllTypes();
    this.createunitAddForm();
    const unitId = window.localStorage.getItem('cUnitId');
    if (!unitId) {
      alert('Invalid action.');
      this.router.navigate(['unit']);
      return;
    } else if (unitId === 'a') {

      this.unitAddForm = this.formBuilder.group({
        id: ['', Validators.required],
        name: ['', Validators.required],
        type: ['', Validators.required],
        opendate: ['', Validators.required],
        remark: ['', Validators.required],
        attachedunit: ['0', Validators.required],
        typeid: ['', Validators.required]
      });
    } else {
    this.unitService.getUnitOne(unitId)
      .subscribe( data => {
        this.unitAddForm = this.formBuilder.group({
          id: ['', Validators.required],
          name: ['', Validators.required],
          type: ['', Validators.required],
          opendate: ['', Validators.required],
          remark: ['', Validators.required],
          attachedunit: [data.id, Validators.required],
          typeid: ['', Validators.required]
        });

      });
  }



}


}
