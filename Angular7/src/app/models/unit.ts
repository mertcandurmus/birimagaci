export class Unit {
    id: number;
    type: string;
    name: string;
    opendate: string;
    remark: string;
    attachedunit: string;
    typeid: number;
}
