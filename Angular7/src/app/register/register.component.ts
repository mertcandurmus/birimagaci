import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user';
import { BasicauthenticationService } from '../services/basicauthentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {





  constructor(private formBuilder: FormBuilder, private basicauthenticationService: BasicauthenticationService) { }

  user: User = new User();
  userAddForm: FormGroup;
  showMsg;

  createuserAddForm() {

    this.userAddForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      username: ['', Validators.required]
    });
  }

  saveUser() {
    this.user = Object.assign({}, this.userAddForm.value);
    this.basicauthenticationService.addUser(this.user).subscribe(
      data => {
        this.showMsg = true;
        setTimeout(() => {this.showMsg = false; }, 2500);
      },
      error => console.log(error));

  }

  ngOnInit() {
    this.createuserAddForm();
  }

}
