import { Component, OnInit } from '@angular/core';
import { UnitService } from '../services/unit.service';
import { Unit } from '../models/unit';

@Component({
  selector: 'app-birim-goster',
  templateUrl: './birim-goster.component.html',
  styleUrls: ['./birim-goster.component.css']
})
export class BirimGosterComponent implements OnInit {

  constructor(private unitService: UnitService) { }

  unit: Unit[];
  leastUnit: Unit[];

  ngOnInit() {
    this.getLeastUnit(0);
  }

 // *ngIf="leastUnit"

  birimGor() {
    this.unitService.getUnits().subscribe(data => {
      this.unit = data;
    });
  }

  getLeastUnit(id: number) {
    this.unitService.getLeastUnit(id).subscribe(data => {
      this.leastUnit = data;
    });
  }



}


/*



 <table  *ngIf="leastUnit" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Birim adı</th>
                            <th>Birim Türü</th>
                            <th>Birim Açılış Tarihi</th>
                            <th>Açıklamalar</th>
                            <th>Uygulama</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr *ngFor="let unt of leastUnit" >
                            <td>{{unt.id}}</td>
                            <td>{{unt.name}}</td>
                            <td>{{unt.type}}</td>
                            <td>{{unt.opendate}}</td>
                            <td>{{unt.remark}}</td>
                            <td><button (click)="getLeastUnit(unt.id)">Alt Birimler</button></td>   
                        </tr>
                    </tbody>
                </table>






                        <table class="table table-striped table-bordered">
            <thead>
                <tr >
                    <th>id</th>
                    <th>Birim adı</th>
                    <th>Birim Türü</th>
                    <th>Birim Açılış Tarihi</th>
                    <th>Açıklamalar</th>
                    <th>Uygulama</th>
                </tr>
            </thead>
            <tbody>
                <tr *ngFor="let unt of unit" >
                    <td>{{unt.id}}</td>
                    <td>{{unt.name}}</td>
                    <td>{{unt.type}}</td>
                    <td>{{unt.opendate}}</td>
                    <td>{{unt.remark}}</td>
                    <td><button (click)="getLeastUnit(unt.id)">Alt Birimler</button></td> 
                </tr>
            </tbody>
        </table>
*/
