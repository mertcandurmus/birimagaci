import { Component, OnInit } from '@angular/core';
import { Unit } from '../models/unit';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UnitService } from '../services/unit.service';


@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private service: UnitService) { }

  unit: Unit;
  showMsg;

  editForm: FormGroup = new FormGroup({
    id: new FormControl(),
    name: new FormControl(),
    type: new FormControl(),
    opendate: new FormControl(),
    remark: new FormControl(),
    attachedunit: new FormControl(),
    typeid: new FormControl()
 });


  ngOnInit() {

    const unitId = window.localStorage.getItem('editUnitId');
    if (!unitId) {
      alert('Invalid action.');
      this.router.navigate(['unit']);
      return;
    }
    this.service.getUnitOne(unitId)
      .subscribe( data => {
        console.log(data);
        this.editForm = this.formBuilder.group({
          id: [data.id, Validators.required],
          name: [data.name, Validators.required],
          type: [data.type, Validators.required],
          opendate: [data.opendate, Validators.required],
          remark: [data.remark, Validators.required],
          attachedunit: [data.attachedunit, Validators.required],
          typeid: [data.typeid, Validators.required]
        });

      });
  }



  onSubmit() {
    this.service.updateUnit(this.editForm.value).subscribe(
      data => {
        console.log(data);
        this.showMsg = true;
        setTimeout(() => {this.showMsg = false; this.router.navigate(['unitUpdate']); }, 2500);
      },
      error => console.log(error));

  }
}
