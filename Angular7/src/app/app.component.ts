import { Component, OnInit } from '@angular/core';
import { BasicauthenticationService } from './services/basicauthentication.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  title = 'FrontEndUnitTree';
  authenticated;

  constructor(private router: Router,
              private basicAuthenticationService: BasicauthenticationService) { }

              ngOnInit() {
              this.authenticated = this.basicAuthenticationService.isUserLoggedIn();
              }

control() {
  this.router.navigate(['login']);
 // this.basicAuthenticationService.isUserLoggedIn();
 // setTimeout(() => {this.authenticated = true; this.control3(); }, 3500);
}


control2() {
  this.router.navigate(['logout']);
 // this.basicAuthenticationService.isUserLoggedIn();
 // setTimeout(() => {this.authenticated = false; this.control3(); }, 3500);
}

}
