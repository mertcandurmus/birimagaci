import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {map, tap, catchError} from 'rxjs/operators';
import { API_URL } from 'src/app.constants';
import { User } from '../models/user';
import { Observable, throwError } from 'rxjs';




export const TOKEN = 'token';
export const AUTHENTICATED_USER = 'authenticatedUser';

@Injectable({
  providedIn: 'root'
})
export class BasicauthenticationService {
  token1: any;
  token2;



  constructor(private http: HttpClient) {
   }

   path = 'http://localhost:8080/api/auth/signup';
   path2 = 'http://localhost:8080/api/auth/signout';


   loadToken() {
    this.token2 = localStorage.getItem('token1');
   }
// Security Bilgilerini alıyoruz
   executeJWTAuthenticationService(username, password) {
   return this.http.post<any>(
     `${API_URL}/api/auth/signin`, {
     username,
     password
    }).pipe(
       map(
         data => {
           sessionStorage.setItem(AUTHENTICATED_USER, username);
           this.token1 = `Bearer ${data.accessToken}`;
           window.localStorage.removeItem('token1');
           window.localStorage.setItem('token1', this.token1);
           sessionStorage.setItem(TOKEN, `Bearer ${data.accessToken}`);
           return data;
         }
       )
     );
}

addUser(user: User): Observable<User> {

  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  return this.http.post<User>(this.path, user, httpOptions).pipe(
    tap(data => console.log(JSON.stringify(data))),
    catchError(this.handleError)
  );
}





handleError(err: HttpErrorResponse) {
  let errorMessage = '';
  if (err.error instanceof ErrorEvent) {
    errorMessage = 'bir hata oluştu' + err.error.message;
  } else {
    errorMessage = 'sistemsel bir hata';
  }

  return throwError(errorMessage);
}






// aldığımız username ve password bilgilerini servis tarafına aktarıyoruz
executeAuthenticationService(username, password) {
  // Security Bilgilerini alıyoruz

  const basicAuthHeaderString = 'Basic ' + window.btoa(username + ':' + password);

  // Servisin header alanına security bilgilerimizi gönderiyoruz
  const headers = new HttpHeaders({
    Authorization: basicAuthHeaderString
  });

  return this.http.get<AuthenticationBean>(

   `${API_URL}/api/auth/signin`,
   {headers}).pipe(
     map(
       data => {
         sessionStorage.setItem(AUTHENTICATED_USER, username);
         sessionStorage.setItem(TOKEN, basicAuthHeaderString);
         return data;
       }
     )
   );
}




getAuthenticatedUser() {
  return sessionStorage.getItem(AUTHENTICATED_USER);
}




getAuthenticatedToken() {
  // user boş değilse
  if (this.getAuthenticatedUser()) {
    return sessionStorage.getItem(TOKEN);
  }
}




// session storage da mevcut authenticatedUser varsa getir.
isUserLoggedIn() {
  const user = sessionStorage.getItem(AUTHENTICATED_USER);
  return !(user === null);
}








logout() {
  sessionStorage.removeItem(AUTHENTICATED_USER);
  sessionStorage.removeItem(TOKEN);
  this.http.post<any>(this.path2, 1);
}


}




export class AuthenticationBean {
  constructor(public message: string) {
  }
}
