import { Injectable } from '@angular/core';
import { BasicauthenticationService } from '../basicauthentication.service';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';


//  Bu servis BasicAuthenticationService işleminden sonra login olunmuşsa ilgili user  ve token bilgilerini
//  Servise header olarak göndermekte ve veri alışverişi için izin almakta

/*Intercepter kullanmamızın nedeni
Başvurunuzda, uygulamanıza sunucuya yapılan tüm istekleri kontrol edip 
sunucudan gelen tüm yanıtları kontrol edebileceğiniz ortak bir yer istiyorsanız, 
en iyi yöntem INTERCEPTOR kullanmaktır.
*/


@Injectable({
  providedIn: 'root'
})
export class HttpIntercepterBasicAuthService {

  constructor(private basicAuthService: BasicauthenticationService) { }





  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const basicAuthHeaderString = this.basicAuthService.getAuthenticatedToken();
    const username = this.basicAuthService.getAuthenticatedUser();

    if (basicAuthHeaderString && username) {

    request = request.clone({
      setHeaders : {
        Authorization : basicAuthHeaderString
      },
      });
    }
    return next.handle(request);
}




}
