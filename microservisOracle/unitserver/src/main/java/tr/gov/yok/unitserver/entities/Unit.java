package tr.gov.yok.unitserver.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import tr.gov.yok.unitserver.entities.audit.UnitDateAudit;

@Entity
@Table(name = "unit")
public class Unit extends UnitDateAudit {

	private static final long serialVersionUID = -3007834787244556562L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE_NAME2")
	@SequenceGenerator(name = "SEQUENCE_NAME2", sequenceName = "SEQUENCE_NAME2", allocationSize = 1, initialValue = 5) // MYSQL=IDENTITY//ORACLE=SEQUENCE
	private int id;

	@Column(name = "name")
	private String name;
	
	private String type;

	@Column(name = "OPEN_DATE")
	private Date opendate;

	@Column(name = "remark")
	private String remark;

	@Column(name = "PARENT_ID")
	private int attachedunit;

	@Column(name = "TYPE_ID")
	private int typeid;

	@ManyToOne
	@JoinColumn(name = "TYPE_ID", insertable = false, updatable = false)
	private UnitType unitType;

	// hibernate need empty constructor
	public Unit() {

	}



	public Unit(int id, String name, Date opendate, String remark, int attachedunit, int typeid) {
		super();
		this.id = id;	
		this.name = name;
		this.opendate = opendate;
		this.remark = remark;
		this.attachedunit = attachedunit;
		this.typeid = typeid;
	}



	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}



	public int getTypeid() {
		return typeid;
	}

	public void setTypeid(int typeid) {
		this.typeid = typeid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getOpendate() {
		return opendate;
	}

	public void setOpendate(Date opendate) {
		this.opendate = opendate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getAttachedunit() {
		return attachedunit;
	}

	public void setAttachedunit(int attechedunit) {
		this.attachedunit = attechedunit;
	}

	public UnitType getUnitType() {
		return unitType;
	}

	public void setUnitType(UnitType unitType) {
		this.unitType = unitType;
	}

}
