package tr.gov.yok.unitserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnitserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnitserverApplication.class, args);
	}

}
