package tr.gov.yok.unitserver.config;

 
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import tr.gov.yok.unitserver.restapi.User;

@Component
public class jwtTokenValidation {
	private static final Logger logger = LoggerFactory.getLogger(jwtTokenValidation.class);

	@Value("${app.jwtSecret}")
	private String jwtSecret;

	@Value("${app.jwtExpirationInMs}")
	private int jwtExpirationInMs;



	public Long getUserIdFromJWT(String token) {
		Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();

		return Long.parseLong(claims.getSubject());
	}

	public String getUsername(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
	}
	

	public UsernamePasswordAuthenticationToken getAuthentication(String token) {
		
			  User user = null;		 		 
			  String url ="http://localhost:8080/getauth2";
			  RestTemplate restTemplate = new RestTemplate();
		      HttpHeaders headers =  new  HttpHeaders();
		      headers.set("Authorization", "Bearer " + token);		  
		      HttpEntity<Object> entity =  new  HttpEntity<Object>(headers);
		      ResponseEntity<?> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
		      String str   =  (String) responseEntity.getBody();
		   
		      ObjectMapper obj=new ObjectMapper();		      
		      try {
				User usr= obj.readValue(str, User.class);
				user= usr;
				System.out.println("----------**----");
				System.out.println(usr);
				System.out.println("----------**----");
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
		UserDetails principal = user;    //new User(getUsername(token), "",authorities);
		
        UsernamePasswordAuthenticationToken t
        = new UsernamePasswordAuthenticationToken(principal, "", principal.getAuthorities());
        
        return t;
	}


	public String resolveToken(HttpServletRequest req) {
		String bearerToken = req.getHeader("Authorization");
		if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

	public boolean validateToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException ex) {
			logger.error("Invalid JWT signature");
		} catch (MalformedJwtException ex) {
			logger.error("Invalid JWT token");
		} catch (ExpiredJwtException ex) {
			logger.error("Expired JWT token");
		} catch (UnsupportedJwtException ex) {
			logger.error("Unsupported JWT token");
		} catch (IllegalArgumentException ex) {
			logger.error("JWT claims string is empty.");
		}
		return false;
	}
	public List<GrantedAuthority> getRoles(String authToken) {
		 List<GrantedAuthority>  authorities= Arrays.asList(Jwts.parser().setSigningKey(jwtSecret)
				.parseClaimsJws(authToken).getBody().get("auth").toString()).stream()
		        .map(authority -> new SimpleGrantedAuthority(authority))
			    .collect(Collectors.toList());

		return authorities;
		}	

	public boolean validateToken2(String token) {
		try {
			Jws<Claims> claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
			if (claims.getBody().getExpiration().before(new Date())) {
				return false;
			}
			return true;
		} catch (JwtException | IllegalArgumentException e) {
			throw new JwtException("Expired or invalid JWT token");
		}
	}

	public String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject);
	}

	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}

	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
	}
}





//
//Claims claims = Jwts.parser()
//           .setSigningKey(jwtSecret)
//           .parseClaimsJws(token)
//           .getBody();
//List<GrantedAuthority>  authorities= Arrays.asList(claims.get("auth").toString().split(",")).stream()
//       .map(authority -> new SimpleGrantedAuthority(authority))
//	    .collect(Collectors.toList());
 

