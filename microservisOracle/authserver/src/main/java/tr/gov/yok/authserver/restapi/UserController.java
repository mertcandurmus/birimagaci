package tr.gov.yok.authserver.restapi;

import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import tr.gov.yok.authserver.dataaccess.RoleRepository;
import tr.gov.yok.authserver.dataaccess.UserRepository;
import tr.gov.yok.authserver.entity.Role;
import tr.gov.yok.authserver.entity.RoleName;
import tr.gov.yok.authserver.entity.User;
import tr.gov.yok.authserver.exception.AppException;
import tr.gov.yok.authserver.payload.ApiResponse;
import tr.gov.yok.authserver.payload.JwtAuthenticationResponse;
import tr.gov.yok.authserver.payload.LoginRequest;
import tr.gov.yok.authserver.payload.SignUpRequest;
import tr.gov.yok.authserver.security.JwtTokenProvider;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	JwtTokenProvider tokenProvider;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		
		SecurityContextHolder.getContext().setAuthentication(null);
						Authentication authentication = 
						authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getusername(), loginRequest.getPassword()));

						SecurityContextHolder.getContext().setAuthentication(authentication);
						String jwt = tokenProvider.generateToken(authentication);
						return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
					
		
					
	
	}

	@PostMapping("/signout")
	@CrossOrigin(origins = "*")
	public void signout( HttpServletRequest request) {
		HttpSession session = request.getSession();
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (session != null) {
			session.invalidate();
			SecurityContextHolder.getContext().setAuthentication(null);
			}
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {

		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<Object>(new ApiResponse(false, "kullanici adi zaten alinmis!"), HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<Object>(new ApiResponse(false, "email adres zaten alinmis"), HttpStatus.BAD_REQUEST);
		}
		User user = new User(signUpRequest.getName(), signUpRequest.getSurname(), signUpRequest.getUsername(), signUpRequest.getEmail(), signUpRequest.getPassword());
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		Role userRole = roleRepository.findByName(RoleName.ROLE_USER).orElseThrow(() -> new AppException("User Role ayarlanamadı."));
		user.setRoles(Collections.singleton(userRole));
		userRepository.save(user);
		System.out.println(user.getRoles());

		return ResponseEntity.ok(new ApiResponse(true, "Kullanıcı başarılı bir şekilde kaydedildi"));
	}

	
	@GetMapping("/user/{username}")
	@CrossOrigin(origins = "*")
	public User getByUsername(@PathVariable String username) {
		return this.userRepository.findByUsername(username);

	}
	
	@PostMapping("/users/{username}")
	public User deleteByUsername(@PathVariable String username) {
		return this.userRepository.deleteUsers(username);

	}
}


