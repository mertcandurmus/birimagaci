package tr.gov.yok.authserver.entity;

public enum RoleName {
	ROLE_USER, ROLE_ADMIN
}
