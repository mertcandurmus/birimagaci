package tr.gov.yok.authserver.dataaccess;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tr.gov.yok.authserver.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);

	User findByUsernameOrEmail(String username, String email);

	List<User> findByIdIn(List<Long> userIds);

	User findByUsername(String username);
	

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);

	@Modifying
	@Query("delete from User b where b.username=:username")
	User deleteUsers(@Param("username") String username);
}
