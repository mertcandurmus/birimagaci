package tr.gov.yok.authserver.controller;



import java.net.URI;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import tr.gov.yok.authserver.entity.User;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("test")
//@AutoConfigureMockMvc
public class UserRestControllerUnitTest {
    

    @Test
    public void givenUser_ThenRegisterUser_thenStatus200() throws Exception {
   
    	RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:"+8080+"/signup";
        URI uri = new URI(baseUrl);
        User user = new User("Adam", "Adam", "Gilly1", "test1@email.com","Gilly1");         
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "");     
        HttpEntity<Object> request =  new  HttpEntity<Object>(user,headers);         
        ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);        
        //Verify request succeed
        Assert.assertEquals(200, result.getStatusCodeValue());
    }
    
    @Test
    public void givenLoginInfoThenLoginSuccesfullyThenReturnToken() throws Exception {
   
    	RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:"+8080+"/signin";
        URI uri = new URI(baseUrl);        
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "");  
        JSONObject jSONObject = new JSONObject("{'username':'admin','password':'admin'}");
        HttpEntity<Object> request =  new  HttpEntity<Object>(jSONObject,headers);         
        ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);        
        //Verify request succeed
        Assert.assertEquals(200, result.getStatusCodeValue());
    }
}
