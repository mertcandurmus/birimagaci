package tr.gov.yok.authserver.dataaccess;

import static org.assertj.core.api.Assertions.assertThat;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import tr.gov.yok.authserver.entity.User;


@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class UserRepositoryIntegrationTest {
   
	@Autowired
    private UserRepository userRepository;
    
    
    @Test
    public void whenFindByEmail_thenReturnUser() {
        // given
        User user = new User("alex","alex","alex","alex@gmail.com","alex");
        userRepository.save(user);
        System.out.println(user.getEmail());
        // when
        User found = userRepository.findByEmail(user.getEmail());
     
        // then
        assertThat(found.getName())
          .isEqualTo(user.getName());
    }
    
    
    @Test
    public void whenFindByUsername_thenReturnUser() {
        // given
        User user = new User("alex","alex","alex","alex@gmail.com","alex");
        userRepository.save(user);
        // when
        User found = userRepository.findByUsername(user.getUsername());    
        // then
        assertThat(found.getUsername())
          .isEqualTo(user.getUsername());
    }
    
    
    @Test
    public void whenexistsByUsername_thenReturnBoolean() {
        // given
        User user = new User("alex","alex","alex","alex@gmail.com","alex");
        userRepository.save(user);
        // when
        Boolean found = userRepository.existsByUsername(user.getUsername());    
        // then
        assertThat(found)
        .isEqualTo(true);
    }
    
    @Test
    public void whenexistsByEmail_thenReturnBoolean() {
        // given
        User user = new User("alex","alex","alex","alex@gmail.com","alex");
        userRepository.save(user);
        // when
        Boolean found = userRepository.existsByEmail(user.getEmail());    
        // then
        assertThat(found)
        .isEqualTo(true);
    }
    
}
